import React, { useState } from 'react';
import { TextInput, Button, View, StyleSheet } from 'react-native';
import { fetchGitHubRepo } from '../api';
import colors from '../colors';

/**
 *
 * @param onSearchResult - function that accepts results from GitHub API call
 * @param onLoadingStateChange - function that accepts a boolean indicating if loading is in progress
 */
function Search({ onSearchResult, onLoadingStateChange }) {
  const [text, updateText] = useState('');

  async function fetchItems() {
    onLoadingStateChange(true);
    const result = await fetchGitHubRepo(text);
    onSearchResult && onSearchResult(result);
    onLoadingStateChange(false);
  }

  return (
    <View style={styles.container}>
      <TextInput
        style={styles.searchInput}
        onChangeText={updateText}
        value={text}
      />
      <View style={styles.searchButton}>
        <Button
          testID="search-button"
          color={colors.text1}
          title="search"
          onPress={fetchItems}
        />
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: 12,
  },
  searchButton: {
    flex: 1 / 4,
  },
  searchInput: {
    flex: 3 / 4,
    backgroundColor: colors.white,
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 4,
  },
});

export default Search;
