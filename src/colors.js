export default {
  backgroundColor: '#faf4f0',
  accent1: '#ebc16e',
  accent2: '#f5b12c',
  accentGrey: '#c5c5c5',
  text1: '#43423a',
  text2: '#4e4e4e',
  white: '#ffffff',
  textError: '#f94520',
  iconHeart: '#900',
};
